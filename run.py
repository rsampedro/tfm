from app import __create_app, socketio

app = __create_app()

if __name__ == '__main__':
    socketio.run(app)
