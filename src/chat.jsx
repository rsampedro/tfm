import React, { useEffect, useState } from "react";
import ReactDOM from 'react-dom';
import "./css/chat.scss"

import { ChatWindow } from "components/chat/chatWindow.jsx";


const root = document.getElementById('root');
let chat = <ChatWindow room_id={room_id} encrypted={encrypted}/>;

ReactDOM.render(chat, root);
