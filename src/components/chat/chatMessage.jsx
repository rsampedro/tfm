import React from 'react';
import {
    Col, Row, Button
} from 'reactstrap';
import { WSClient } from "aux/socket.jsx"


class MessageComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    getMessageClass(sent_by) {
        if (sent_by == WSClient.socket_id) { return ("own_message") } else { return ("other_message") }
    }

    render() {
        var message_type = this.getMessageClass(this.props.sent_by) + " message_row";
        return (
            <Row className={message_type}>
                <Col xs="2" className="sent_by">{this.props.sent_by}</Col>
                <Col xs="8" className="message_body">{this.props.message_body.message}</Col>
                <Col xs="2" className="timestamp">{this.props.timestamp}</Col>
            </Row>
        );
    }
}

class SystemMessageComponent extends MessageComponent {
    getMessageClass(sent_by) {
        return "system_message"
    }
}

class FileOfferComponent extends React.Component {
    constructor(props) {
        super(props);
        this.filename = props.message_body.filename
        this.file_owner = props.sent_by
        this.file_hash = props.message_body.file_hash
        this.file_size = props.message_body.file_size
        this.state = {
            disabled: false
        }

        this.onAcceptFile = this.onAcceptFile.bind(this)
    }

    getMessageClass(sent_by) {
        if (sent_by == WSClient.socket_id) { return ("own_message") } else { return ("other_message") }
    }

    onAcceptFile(event) {
        if (event.target.disabled) {
            return
        }

        var message_data = {
            message: WSClient.socket_id + " has accepted the file with Hash: " + this.file_hash,
            file_hash: this.file_hash,
        }

        let header_data = {
            to: this.file_owner
        }
        WSClient.received_file_hash = this.file_hash
        WSClient.received_file_chunks = {}
        WSClient.emit("accepted_file", message_data, header_data)

        var chunk_data = {
            chunk_id: 0,
            file_hash: this.file_hash,
        }
        WSClient.emit("request_chunk", chunk_data, header_data)
        this.setState({ disabled: true })
    }

    render() {
        let message = "User <"+this.file_owner+"> offers the file: "+this.filename+" of size: "+this.file_size+" and hash: "+this.file_hash
        var message_type = this.getMessageClass(this.props.sent_by) + " file_offer";
        return (
            <Row className={message_type}>
                <Col xs="2" className="sent_by">{this.props.sent_by}</Col>
                <Col xs="7" className="message_body">{message}</Col>
                <Col xs="1" className="message_button">
                    <Button color="primary" onClick={this.onAcceptFile} disabled={this.state.disabled}>Accept</Button>
                </Col>
                <Col xs="2" className="timestamp">{this.props.timestamp}</Col>
            </Row>
        )
    }
}

export { MessageComponent, SystemMessageComponent, FileOfferComponent }