import React from 'react';
import {
    Col, Row, Container, Form, FormGroup, Input, Button, InputGroup
} from 'reactstrap';

import { MAX_FILESIZE } from "aux/constants.jsx"

import { WSClient } from "aux/socket.jsx"


class ChatInputBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            message: "",
            selected_file: null,
            file_select_disabled: false,
        }
        this.handleSubmit = this.handleSubmit.bind(this)
        this.onInputChange = this.onInputChange.bind(this)
    }

    onInputChange(event){
        this.setState({message: event.target.value})
    }

    onFileButton(event){
        if (WSClient.shared_file != null){
            event.target.disabled = true
            this.setState({file_select_disabled:true})
            return;
        }

        const file = event.target.files[0];
        if (file.size > MAX_FILESIZE){
            alert("Filesize limit is set at "+MAX_FILESIZE+" bytes")
            return
        }
        WSClient.readFile(file)

        var message_data = {
            filename: file.name,
            file_size: file.size,
            file_hash: WSClient.shared_file_hash,
        }
        WSClient.emit("offer_file", message_data)

        event.target.disabled = true
        this.setState({file_select_disabled:true})
    }

    handleSubmit(event) {
        event.preventDefault();
        var trimmed_message = this.state.message.trim();
        if (trimmed_message == ""){
            return false;
        }
        var data = {
            message: trimmed_message,
        }

        WSClient.emit("message", data);
        this.setState({ "message": "" });
    }

    render() {
        let file_selector = ""
        if (!WSClient.is_encrypted){
            file_selector = <Input name="file_select" type="file" disabled={this.state.file_select_disabled} value={this.state.selected_file} onChange={() => this.onFileButton(event)}/>
        }
        return (
            <Row>
                <Form onSubmit={this.handleSubmit}>
                    <InputGroup id="chatBox">
                        <Input name="message" value={this.state.message} onChange={() => this.onInputChange(event)} />
                        <Button color="primary" type="submit" >Send</Button>
                    </InputGroup>
                </Form>
            {file_selector}
            </Row>
        );
    }
}

export { ChatInputBar }