import React from 'react';
import {
    Col, Row, Container
} from 'reactstrap';
import { ChatInputBar } from 'components/chat/chatInputBar.jsx'
import { generateMessageComponent } from 'aux/eventHandling.jsx'


import { WSClient } from "aux/socket.jsx"


class ChatWindow extends React.Component {
    constructor(props) {
        super(props);
        this.room_id = props.room_id;
        this.state = {
            joined: false,
            encrypted: props.encrypted,
            event_list: [],
            username: props.username || ""
        }
        this.addMessageComponentToEventList = this.addMessageComponentToEventList.bind(this)
        this.handleSocketIOEvents = this.handleSocketIOEvents.bind(this)

        WSClient.is_encrypted = props.encrypted;
        WSClient.chat_window = this;
    }

    handleSocketIOEvents(event_type, message_data){
        console.debug("Received event "+event_type+" with data"+message_data)
        let entry_info = WSClient.handleEvents(event_type, message_data)
        entry_info["event_type"] = event_type
        this.addMessageComponentToEventList(entry_info);
    }

    addMessageComponentToEventList(event_data){
        var new_event_component = generateMessageComponent(event_data)
        if (new_event_component == null){
            console.debug("Couldn't render message type "+event_data["event_type"])
            return;
        }
        var current_message_list = this.state.event_list
        current_message_list.push(new_event_component)
        this.setState({event_list:current_message_list})
    }

    async componentDidMount() {
        let logged_in = await WSClient.join(this.room_id)
        this.setState({"joined": logged_in})
        if (logged_in == true){
            WSClient.client.on("has_joined", (message_data) => this.handleSocketIOEvents("has_joined", message_data));
            WSClient.client.on("introduce_public_key", (message_data) => this.handleSocketIOEvents("introduce_public_key", message_data));
            WSClient.client.on("reply_public_key", (message_data) => this.handleSocketIOEvents("reply_public_key", message_data));
            WSClient.client.on("message", (message_data) => this.handleSocketIOEvents("message", message_data));
            WSClient.client.on("system_message", (message_data) => this.handleSocketIOEvents("system_message", message_data));
            WSClient.client.on("offer_file", (message_data) => this.handleSocketIOEvents("offer_file", message_data));
            WSClient.client.on("request_chunk", (message_data) => this.handleSocketIOEvents("request_chunk", message_data));
            WSClient.client.on("send_chunk", (message_data) => this.handleSocketIOEvents("send_chunk", message_data));
            WSClient.client.on("transfer_complete", (message_data) => this.handleSocketIOEvents("transfer_complete", message_data));
            
        }
        if (WSClient.is_encrypted){
            let sent_key = await WSClient.introduce_public_key(this.room_id)
        }
    }

    renderChatHeader() {
        return (
            <Row className="chat_header">
                <Col xs="4">Room Id: {this.props.room_id}</Col>
                <Col xs="4">Socket Id: {WSClient.socket_id}</Col>
                <Col xs="4">
                    Username: {this.state.username}
                </Col>
                
            </Row>
        )
    }

    renderMessages() {
        if (this.state.event_list == undefined) {
            return null
        }else{
            return this.state.event_list
        }
    }

    render() {
        if (this.state.joined == true){
            return (
                <Container className="chat_window">
                    {this.renderChatHeader()}
                    <Container className="message_list">
                        {this.renderMessages()}
                    </Container>
                    <ChatInputBar/>
                </Container>
            )    
        }else{
            return(
            <Container className="chat_window">
                Couldn't join room
            </Container>
            )
        }
    }
}
export { ChatWindow }