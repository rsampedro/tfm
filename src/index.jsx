import React, { useEffect, useState } from "react";
import {
    Col, Row, Container, Form, FormGroup, InputGroup, Input, Button
} from 'reactstrap';
import ReactDOM from 'react-dom';
import "./css/chat.scss"


class MainWindow extends React.Component {
    constructor(props) {
        super(props);
    }

    onCreateRoom(is_encrypted){
        window.location.href = (is_encrypted)? "private" : "room"
    }

    onSubmitForm(event, is_encrypted){
        event.preventDefault();
        console.log(event)
        console.log(is_encrypted)
        var new_url = event.target.form_input.value.trim();
        if (new_url == ""){ 
            alert("Cannot enter room with no id"); 
            return
        }
        else {
            let namespace = (is_encrypted)? "private" : "room"
            window.location.href = namespace+"/"+new_url
        }

    }

    getChatForm(is_encrypted=false){
        return(
            <Container>
                <Row>
                    <Col>
                        <h2>
                        {((is_encrypted)? "Encrypted":"Unencrypted")} Chat
                        </h2>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <FormGroup row>
                            <Col>
                                <Button onClick={() => this.onCreateRoom(is_encrypted)}>Create new room</Button>
                            </Col>
                        </FormGroup>
                    </Col>
                    <Col>
                        <Form id={((is_encrypted)? "encrypted":"")+"_form"} onSubmit={(event) => this.onSubmitForm(event, is_encrypted)} >
                            <FormGroup row>
                                <Col>
                                    <Input name="form_input" placeholder="Insert Room Id"/>
                                </Col>
                                <Col>
                                    <Button type="submit">Join Room</Button>
                                </Col>
                            </FormGroup>
                        </Form>
                    </Col>
                </Row>
            </Container>
        )
    }


    render() {
        return (
            <Container className="MainMenu">
                <Row>
                    <Col>
                        <h1>
                            Chat
                        </h1>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        {this.getChatForm(false)}
                    </Col>
                </Row>
                <Row>
                    <Col>
                        {this.getChatForm(true)}
                    </Col>
                </Row>

            </Container>
        )
    }
}



const root = document.getElementById('root');
let chat = <MainWindow />;

ReactDOM.render(chat, root);
