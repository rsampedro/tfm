function castEventValue(event, type){
    var value = null
    var process_function = null
    switch (type){
        case 'int':
            process_function = parseInt;
        case 'float':
            process_function = parseFloat;
    }
    if (process_function) {
        value = process_function(event.target.value)
    }else{
        value = event.target.value
    }
    return value
}

function handleInputData(event, type=null){
    var value = null
    var process_function = null
    switch (type){
        case 'int':
            process_function = parseInt;
        case 'float':
            process_function = parseFloat;
    }
    if (process_function) {
        value = process_function(event.target.value)
    }else{
        value = event.target.value
    }

    this.setState({
        [event.target.name] : value
    })
}

export {handleInputData}