import { WSClient } from "aux/socket"
import * as forge from 'node-forge'
import { FILE_CHUNK_SIZE } from "aux/constants"

var handleEvents = {
    "open": rejoin,
    "request_chunk": send_file_chunk,
    "send_chunk": process_file_chunk,
    "transfer_complete": finish_transfer,
    "introduce_public_key": receive_presented_public_key,
    "reply_public_key": receive_public_key,
    "has_joined": user_has_joined
}

var plainEvents = [
    "introduce_public_key",
    "reply_public_key",
    "has_joined",
    "open"
]

function rejoin(socket_obj){
    console.log("Server opened connection, rejoining room...")
    socket_obj.join(client.room_id)
}

function user_has_joined(socket_obj, event_params){
    socket_obj.other_socket_id = event_params.message_body.socket_id
}

function receive_presented_public_key(socket_obj, event_params){
    let message_body = event_params.message_body
    console.log("Received public key, replying...")
    let received_key = message_body["public_key"]
    socket_obj.public_keys[event_params["sent_by"]] = forge.pki.publicKeyFromPem(received_key)

    let response_header = {to:event_params.sent_by}

    message_body = {
        "public_key": forge.pki.publicKeyToPem(socket_obj.own_keys.publicKey),
        "message": "Received public key, replying..."
    }

    socket_obj.emit("reply_public_key", message_body, response_header);
}

function receive_public_key(socket_obj, event_params){
    console.log("Received public key reply, saving...")
    let message_body = event_params.message_body
    let received_key = message_body["public_key"]
    socket_obj.other_socket_id = event_params["sent_by"]
    socket_obj.public_keys[event_params["sent_by"]] = forge.pki.publicKeyFromPem(received_key)
}

function send_file_chunk(socket_obj, event_params){
    let message_body = event_params.message_body
    var chunk_id = message_body["chunk_id"];
    var start_byte = chunk_id * FILE_CHUNK_SIZE
    var end_byte = start_byte + FILE_CHUNK_SIZE

    var byteArray = socket_obj.shared_fileByteArray
    if ( end_byte > byteArray.length){
        end_byte = byteArray.length
    }
    console.log("Sending file chunk "+chunk_id+" Bytes ["+start_byte+" : "+end_byte+"]")

    let message_header = {
        to: event_params.sent_by
    }
    
    var response_body = {
        file_size: socket_obj.shared_fileByteArray.length,
        chunk_id: chunk_id,
        file_hash: message_body["file_hash"],
        file_name: socket_obj.shared_file.name,
        binary_data: socket_obj.shared_fileByteArray.slice(start_byte, end_byte)
    }
    socket_obj.emit("send_chunk", response_body, message_header)

}

function process_file_chunk(socket_obj, event_params){
    let message_body = event_params.message_body
    var chunk_id = message_body["chunk_id"];
    var max_chunks = parseInt(message_body["file_size"] / FILE_CHUNK_SIZE)
    console.log("Receiving file chunk "+chunk_id)
    socket_obj.received_file_chunks[chunk_id] = message_body["binary_data"]

    var response_body = {}
    let response_header = {to: event_params.sent_by}

    if (Object.keys(socket_obj.received_file_chunks).length == max_chunks + 1){
        response_body = {
            file_hash: message_body["file_hash"],
            message: "Received last chunk of file <"+message_body["file_hash"]+">"
        }
        socket_obj.emit("transfer_complete", response_body, response_header)
        socket_obj.buildFile(message_body["file_name"])
        return true;
    }else{
        if (chunk_id > max_chunks){
            console.log("Received Chunk with id too large")
            return false
        }
    }

    response_body = {
        chunk_id: chunk_id + 1,
        file_hash: message_body["file_hash"],
    }
    socket_obj.emit("request_chunk", response_body, response_header)
    return false
}

function finish_transfer(socket_obj, event_params){
    socket_obj.shared_file = null
    socket_obj.shared_fileByteArray = []
    socket_obj.shared_file_hash = ""
    return true
}


export {handleEvents, plainEvents}