/* Constants to reuse all around the project */

export const MAX_FILESIZE = 1024*1024*10 // 10 MB Max filesize
export const FILE_CHUNK_SIZE = 1024 // 1 Kb per message
