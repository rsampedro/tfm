import socketIOClient from "socket.io-client";
import { handleEvents, plainEvents } from "aux/chatEvents.jsx";

import * as forge from 'node-forge'

//import { SHA256 } from "crypto-js"

//const WS_ENDPOINT = "https://tfm-ricard-sampedro.herokuapp.com"
var WS_PROTOCOL = "ws:"
if (window.location.protocol == "https:"){
    WS_PROTOCOL = "wss:"
}

const WS_ENDPOINT = WS_PROTOCOL+"//" + window.location.host
const OPTIONS = { transports: ['websocket'] }


class SocketObj {
    constructor(endpoint, options) {
        this.endpoint = endpoint;
        this.options = options;
        this.client = new socketIOClient(WS_ENDPOINT, OPTIONS);
        this.socket_id = null;
        this.other_socket_id = null;
        this.room_id = null;
        this.eventHandlers = handleEvents;
        this.shared_file = null;
        this.shared_fileByteArray = []
        this.shared_file_hash = ""
        this.received_file_chunks = {}
        this.received_file_hash = null
        this.chat_window = null

        // Encryption
        this.is_encrypted = false;
        this.own_keys = null
        this.public_keys = {}

        // Methods
        this.generateKeys = this.generateKeys.bind(this)
        this.readFile = this.readFile.bind(this)
        this.handleEvents = this.handleEvents.bind(this)
        this.buildFile = this.buildFile.bind(this)
        this.join = this.join.bind(this)
        this.emit = this.emit.bind(this)
        this.encrypt_event = this.encrypt_event.bind(this)
        this.decrypt_event = this.decrypt_event.bind(this)
    }

    generateKeys() {
        var keys = forge.pki.rsa.generateKeyPair({ bits: 2048, e: 0x10001 });
        this.public_keys[self.socket_id] = keys.publicKey
        console.log(keys)
        return keys;
    }

    decrypt_event(event_data) {
        var privateKey = this.own_keys.privateKey;
        var new_data = event_data
        new_data.message_body = JSON.parse(privateKey.decrypt(event_data["message_body"]))
        return (new_data)
    }

    encrypt_event(event_data) {
        let publicKey = this.public_keys[this.other_socket_id]
        var encrypted = publicKey.encrypt(JSON.stringify(event_data))
        return encrypted
    }

    readFile(file) {
        const reader = new FileReader();
        const fileByteArray = [];
        reader.readAsArrayBuffer(file);
        reader.onloadend = (evt) => {
            if (evt.target.readyState === FileReader.DONE) {
                const arrayBuffer = evt.target.result,
                    array = new Uint32Array(arrayBuffer);
                for (const a of array) {
                    fileByteArray.push(a);
                }
                console.log(fileByteArray)
            }
        }
        this.shared_file = file
        this.shared_fileByteArray = fileByteArray
        console.log(this.shared_fileByteArray)
        this.shared_file_hash = forge.md.sha256.create();
        this.shared_file_hash.update(fileByteArray);
        this.shared_file_hash = this.shared_file_hash.digest().toHex();
    }

    buildFile(filename) {
        var filearray = []
        for (let idx = 0; idx < Object.keys(this.received_file_chunks).length; idx++) {
            filearray = filearray.concat(this.received_file_chunks[idx])
        }

        var intarray = new Uint32Array(filearray)
        var asBuffer = intarray.buffer;
        
        console.log(filearray)
        var final_file_hash = forge.md.sha256.create();
        final_file_hash.update(filearray);
        final_file_hash = final_file_hash.digest().toHex();

        /*        
        if (final_file_hash == this.received_file_hash){
            console.log("File hashes match")
        }else{
            alert("File hashes do not match! Something has gone")
            console.log([final_file_hash, this.received_file_hash])
        }*/

        const blob = new Blob([asBuffer]);
        const objectURL = URL.createObjectURL(blob);
        const link = document.createElement('a');
        link.href = objectURL;
        link.href = URL.createObjectURL(blob);
        link.download = filename;
        link.click();
    }

    handleEvents(event, message_data) {
        var event_data = message_data;
        // Decrypt if it's an event type that is encrypted
        if (this.is_encrypted && !plainEvents.includes(event)) {
            event_data = this.decrypt_event(event_data)
        }

        // Execute logic for event
        if (event in this.eventHandlers) {
            console.debug("Handling event: " + event)
            this.eventHandlers[event](this, event_data)
        }
        return event_data
    }

    async join(room_id) {
        if (this.is_encrypted){
            this.own_keys = this.generateKeys();
        }
        let message_data = {
            room: room_id,
        }
        let logged_in = await new Promise(resolve => this.client.emit('join', message_data, response => resolve(response)))
        if (logged_in == true) {
            this.socket_id = this.client.id;
            this.room_id = room_id;
        }
        return logged_in
    }

    async introduce_public_key() {
        console.log("Introducing public key...")
        let public_key = forge.pki.publicKeyToPem(this.own_keys.publicKey)
        let message_data = {
            room: this.room_id,
            timestamp: Date.now(),
            message_body: { public_key: public_key, message:"User <"+this.socket_id+"> is sharing their public key"}
        }

        let response = await new Promise(resolve => this.client.emit('introduce_public_key', message_data, response => resolve(response)))
        return response
    }

    emit(event, event_data, overwrite_header = {}) {
        if (this.room_id != null) {
            // Encrypt message if encryption is enabled
            let processed_data = event_data
            if (this.is_encrypted && !plainEvents.includes(event)) {
                processed_data = this.encrypt_event(processed_data)
                let new_event_data = {
                    timestamp: Date.now(),
                    sent_by: this.socket_id,
                    message_body: event_data,
                }
                new_event_data["event_type"] = event
                this.chat_window.addMessageComponentToEventList(new_event_data)
            }

            let enriched_data = {
                room: this.room_id,
                timestamp: Date.now(),
                message_body: processed_data,
            }
            enriched_data = {
                ...enriched_data,
                ...overwrite_header
            }
            this.client.emit(event, enriched_data)
        }
    }
}

export let WSClient = new SocketObj(WS_ENDPOINT, OPTIONS);
