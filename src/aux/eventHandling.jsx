import { MessageComponent, FileOfferComponent, SystemMessageComponent} from "components/chat/chatMessage.jsx"

const eventComponentMapping = {
    "message": MessageComponent,
    "system_message": SystemMessageComponent,
    "offer_file": FileOfferComponent,
    "transfer_complete": SystemMessageComponent,
    "reply_public_key": MessageComponent,
    "has_joined": SystemMessageComponent,
    "accepted_file": SystemMessageComponent
}

function generateMessageComponent(event_data){
    var component_class = eventComponentMapping[event_data["event_type"]]
    if (component_class === undefined){ return null }
    var component = new component_class(event_data)
    return component.render()
}

export { generateMessageComponent }