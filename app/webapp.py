import shortuuid
from flask import Blueprint, render_template, redirect, url_for, session

app = Blueprint("webapp", "webapp")


@app.route("/")
def main_page():
    return render_template("main.html")


@app.route("/private")
def create_private_room():
    room_id = shortuuid.uuid()
    return redirect(url_for(f"webapp.private_chat_room", room_id=room_id))


@app.route("/private/<room_id>")
def private_chat_room(room_id: str):
    session["room"] = "private_"+room_id
    return render_template("chat.html", encrypted=True, room_id="private_"+room_id)


@app.route("/room")
def create_room():
    room_id = shortuuid.uuid()
    return redirect(url_for(f"webapp.chat_room", room_id=room_id))


@app.route("/room/<room_id>")
def chat_room(room_id: str):
    session["room"] = room_id
    return render_template("chat.html", encrypted=False, room_id=room_id)
