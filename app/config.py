import os
APP_NAME = 'TFM_APP'
ENV = os.getenv('ENV')
DEBUG = os.getenv('DEBUG')
SECRET_KEY = os.getenv('SECRET_KEY')
SESSION_COOKIE_NAME = f'{APP_NAME}_COOKIE'
PORT = os.getenv('PORT')
