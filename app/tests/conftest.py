import pytest
from app import socketio, __create_app


@pytest.fixture()
def flask_app():
    app = __create_app()
    app.config.from_object('app.tests.config')
    yield app


@pytest.fixture()
def socket_client(flask_app):
    yield socketio.test_client(flask_app)
