import pytest

def test_socket_can_connect(socket_client):
    socket_client.send("message")
    assert socket_client.is_connected() is True

