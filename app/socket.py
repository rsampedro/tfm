from flask import request, session, current_app
from flask_socketio import join_room, leave_room, SocketIO, emit, rooms
from datetime import datetime

from app.aux.room_logic import can_join_room, get_all_rooms

socketio = SocketIO(manage_session=True, cors_allowed_origins="*", engineio_logger=True)
ROOM_LIMIT = 2


def emit_system_message(message: str, to: str = None, timestamp: str = None):
    timestamp = timestamp or datetime.timestamp(datetime.now())
    message_data = {
        "sent_by": "SYSTEM",
        "message_body": {"message":message},
        "timestamp": timestamp,
    }
    emit("system_message", message_data, to=to)


def enrich_data(data):
    sender = request.sid
    new_data = data
    new_data.update({
        "sent_by": sender
    })
    return new_data


@socketio.on('join')
def handle_join(data):
    room = data['room']
    if not can_join_room(room):
        emit_system_message("Cannot join the room", to=request.sid)

        if room in get_all_rooms():
            emit_system_message(f"{request.sid} tried to join the room.", to=room)

        return False
    join_room(room)

    new_data = {
        "sent_by": "SYSTEM",
        "message_body": {"socket_id": request.sid,
                         "message": f"User <{request.sid}> has joined the room"}
    }
    emit("has_joined", new_data, to=room)
    return True


@socketio.on('leave')
def handle_leave(data):
    room = data['room']
    leave_room(room)
    current_app.logger.info(f"({request.sid}) left {room}")
    emit_system_message(f'{request.sid} has left the room.', to=room)


@socketio.on('disconnect')
def handle_disconnect():
    current_app.logger.info(f"User <{request.sid}> has disconnected")


@socketio.on('message')
def handle_message(data):
    new_data = enrich_data(data)
    emit("message", new_data, to=data['room'])
    return True


# WIP actually implement
@socketio.on('change_username')
def handle_change_username(data):
    new_username = data["username"]
    session["username"] = new_username
    for room in rooms(request.sid):
        emit("changed_username", {f"Sid <{request.sid}> changed their username to <{new_username}>"}, to=room)


@socketio.on('connect')
def handle_connect():
    current_app.logger.info(f"<{request.sid}> has connected")


@socketio.on('offer_file')
def handle_offer_file(data):
    new_data = enrich_data(data)
    emit("offer_file", new_data, to=new_data["room"], include_self=False)


@socketio.on('accepted_file')
def handle_accepted_file(data):
    new_data = enrich_data(data)
    file_owner = data["to"]
    emit("accepted_file", new_data, to=file_owner)


@socketio.on("send_chunk")
def handle_send_chunk(data):
    new_data = enrich_data(data)
    file_requester = data["to"]
    emit("send_chunk", new_data, to=file_requester, include_self=False)


@socketio.on("request_chunk")
def handle_request_chunk(data):
    new_data = enrich_data(data)
    file_owner = data["to"]
    emit("request_chunk", new_data, to=file_owner, include_self=False)


@socketio.on("transfer_complete")
def handle_file_received(data):
    new_data = enrich_data(data)
    file_owner = data["to"]
    emit("transfer_complete", new_data, to=file_owner, include_self=False)


@socketio.on("introduce_public_key")
def handle_introduce_public_key(data):
    new_data = enrich_data(data)
    emit("introduce_public_key", new_data, to=data["room"], include_self=False)


@socketio.on("reply_public_key")
def handle_reply_public_key(data):
    new_data = enrich_data(data)
    emit("reply_public_key", new_data, to=data["to"], include_self=False)
