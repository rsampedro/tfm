from flask import request, session, current_app
from flask_socketio import join_room, leave_room, SocketIO, emit, rooms

ROOM_LIMIT = 2


def get_room_participants(room_id):
    return list(current_app.extensions["socketio"].server.manager.rooms["/"].get(room_id) or [])


def get_all_rooms():
    return list(current_app.extensions["socketio"].server.manager.rooms["/"])


def can_join_room(room_id):
    room_participants = get_room_participants(room_id)
    if len(room_participants) >= ROOM_LIMIT:
        return False
    return True
