import os

from flask import Flask
from app.socket import socketio


def __create_app():
    static_folder = os.path.join(os.getcwd(), "static")
    flask_app = Flask(__name__, static_folder=static_folder)
    flask_app.config.from_object('app.config')

    socketio.init_app(flask_app)

    from app.webapp import app as webapp_bp

    flask_app.register_blueprint(webapp_bp, url_prefix="")
    from . import socket

    return flask_app


app = __create_app()
