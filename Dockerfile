FROM python:3.8

MAINTAINER Ricard Sampedro "rsampedro92@uoc.edu"

RUN apt-get update -y && \
    apt-get install -y python3-pip python3-dev npm

ENV FLASK_ENV production

COPY ./requirements.txt /app/requirements.txt

WORKDIR /app

RUN pip install -r ./requirements.txt

COPY . /app

RUN npm install
RUN npm run build

EXPOSE 8000
CMD gunicorn --worker-class eventlet -w 1 app:app