# Hidden Service Chat

Aquest servei consisteix en una aplicació de Xat, amb servidor Flask (Python) i client React App, que utilitza SocketIO amb websockets com a transport layer per a l'enviament d'events entre el servidor i client.

Aquest servei està pensat per a poder ser desplegat fàcilment com a servei HTTP o com a Hidden Service a través de Heroku seguint les instruccions següents:

## Desplegament Local (com a servei HTTP)
- Dockerfile

Aquest servei pot ser desplegat com a imatge docker, primer generant la imatge mateixa

```
make build
```

i després executant-la:

```
make run-docker
```

- Local environment

Primer cal instalar les dependències, assegurant-se de tenir la versió de Python i node correctes instal·lades:

```
$ python --version
Python 3.8.13

$ node --version
v17.8.0
```

Llavors, instal·lem dependències:
```
$ make venv
$ make install
```

Finalment, generem els bundles de la app client, activem el virtual environment i iniciem el servidor python:

```
$ npm start
$ venv/bin/python run.py
```

## Desplegament a Heroku

Per a desplegar la aplicació a Heroku, els requeriments són els següents:

### Buildpacks
- heroku/nodejs: Buildpack de Heroku per nodejs
- heroku/python: Buildpack de Heroku per Python (versió especificada dins de runtime.txt)
- https://github.com/tor-actions/heroku-buildpack-tor.git#a7d72930877743ea68d304412ec3acac37bb30e7: Buildpack de Tor Browser, que automatitza el procés de configuració d'aquest

### Tipus de Dyno
Aquesta aplicació està pensada per a poder ser servida com a servei HTTP i com a Hidden Service. Per defecte, independentment del tipus de Dyno, el Hidden Service intentarà arrencar, però si el tipus de Dyno escollit dins de Procfile és de tipus web, llavors el servidor també serà accessible com a servei HTTP.