const path = require('path');
const webpack = require('webpack');

module.exports = {
  entry: {
    index: './src/index.jsx',
    chat: './src/chat.jsx',
    chat_css: './src/css/chat.scss',
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          // Creates `style` nodes from JS strings
          "style-loader",
          // Translates CSS into CommonJS
          "css-loader",
          // Compiles Sass to CSS
          "sass-loader",
        ],
      },
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx', '.css'],
    modules: [
      path.resolve(__dirname + "/node_modules"),
      path.resolve(__dirname + "/src")
    ]

  },
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'static/dist'),
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        WS_ENDPOINT: JSON.stringify(process.env.WS_ENDPOINT)
      }
    }),
  ]
};