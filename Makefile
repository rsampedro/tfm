.PHONY: clean venv install build debug

clean:
	rm -rf dist/
	rm -rf venv/
	rm -rf .pytest_cache

venv:
	python -m venv venv
	@venv/bin/pip install -Uq pip
	@if [ -s dev-requirements.txt ]; then\
		venv/bin/pip install $(pip-extra-args) -qr dev-requirements.txt;\
	else\
		echo "skipping dev-requirements.txt (empty)";\
	fi  # development tools
	venv/bin/pip install -r requirements.txt

install:
	npm install

src-build: install
	npm run build --production

compile-front:
	npm run start

build:
	docker build -t chatapp .

run-docker:
	docker run -p 8000:8000 -i chatapp

