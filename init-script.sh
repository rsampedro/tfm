# Load env
echo "$TOR_PUBLIC_KEY" | base64 --decode > config/hs_ed25519_public_key
echo "$TOR_SECRET_KEY" | base64 --decode > config/hs_ed25519_secret_key
echo $ONION_LOCATION > config/hostname